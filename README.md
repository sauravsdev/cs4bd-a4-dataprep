# CS4BD-A4-DataPrep
### Repository for the assignment A4-Data Preparation  

**Course Name**: Computer Science for Big Data  
**Program**: Masters in Data Science  
**Semester**: Winter 2019-2020  
**Course Teacher**: Prof. Stefan Edlich  
**Contributor**: Saurav Kumar Saha  
#
#
#### Solutions:
**#A (My Kaggle Solutions)**
  
- [Data Cleaning Challenge: Handling missing values](https://www.kaggle.com/sksdotsauravs/data-cleaning-challenge-handling-missing-values "Data Cleaning Challenge: Handling missing values")
- [Data Cleaning Challenge: Scale and Normalize Data](https://www.kaggle.com/sksdotsauravs/data-cleaning-challenge-scale-and-normalize-data "Data Cleaning Challenge: Scale and Normalize Data")
- [Data Cleaning Challenge: Parsing Dates](https://www.kaggle.com/sksdotsauravs/data-cleaning-challenge-parsing-dates "Data Cleaning Challenge: Parsing Dates")
- [Data Cleaning Challenge: Character Encodings](https://www.kaggle.com/sksdotsauravs/data-cleaning-challenge-character-encodings "Data Cleaning Challenge: Character Encodings")  
- [Data Cleaning Challenge: Inconsistent Data Entry](https://www.kaggle.com/sksdotsauravs/data-cleaning-challenge-inconsistent-data-entry "Data Cleaning Challenge: Inconsistent Data Entry")  
  
**#B (Data Wrangling with Trifacta)**
  
-   [data-wrangling-with-trifacta.pdf](https://bitbucket.org/sauravsdev/cs4bd-a4-dataprep/src/master/data-wrangling-with-trifacta.pdf "data-wrangling-with-trifacta.pdf")

#### Task Description
- (A) Do the 5 Day Data Cleaning Challenge by Rachel Tateman: https://www.kaggle.com/getting-started/52652#latest=447933  
      The sign up here link is broken but if you search the 5 courses in Kaggle you will find them easily.  
      Fork her repositories and send me one or five linkes to your kaggle solutions.  
#
- (B) Play around with Trifacta! Import your favourite dataset where you might have inserted some  errors before.  
      Upload one PDF where you show me screenshots, explain the problems with the dataset and show the recipies that solved the problems.  
#
**Deadline**: Monday, 10th February 2020, 11:59pm

#
#
#
#### Reference Links:
 - https://www.kaggle.com/rtatman/data-cleaning-challenge-handling-missing-values
 - https://cloud.trifacta.com
 - https://docs.trifacta.com/display/SS/Common+Tasks